#!/usr/bin/env perl
use Mojolicious::Lite;

use Module::Load;
load 'lib/Mojo/EggdropRPC.pm';

my $rpc = Mojo::EggdropRPC->new(
	password => 'Satan Calculus is coming to town'
);
$rpc->connect(address => '127.0.0.1', port => 1337);

get '/' => sub {
	my $c = shift;
	$rpc->call(['checkpass', 'username-here', 'password-here'] => sub {
		my ($rpc, $msg) = @_;

		my $page = 'error';
		$page = $msg->[1] eq "1" ? 'index' : 'failed' if $msg->[0] eq 'OK';

		$c->render($page);
	});
};

app->start;

__DATA__

@@ error.html.ep
% layout 'default';
% title 'Internal error';
An internal error has occured. Please inform your administrator.

@@ index.html.ep
% layout 'default';
% title 'Welcome';
You are successfully logged in.

@@ failed.html.ep
% layout 'default';
% title 'Login failed';
Login failed.

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>
