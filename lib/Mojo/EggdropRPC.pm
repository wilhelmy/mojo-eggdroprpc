package Mojo::EggdropRPC;

=encoding utf8

=head1 NAME

Mojo::EggdropRPC - Asynchronous eggdrop-rpc client library for Mojolicious

=head1 SYNOPSIS

  use Mojo::EggdropRPC;

  my $rpc = Mojo::EggdropRPC->new(password => 'foo');
  $rpc->connect(address => '127.0.0.1', port => 1337);

  $rpc->call([qw/echo hello world/], sub {
    my ($rpc, $msg) = @_;
    ...
  });

=cut

use vars qw/$VERSION $AUTHOR/;
use Carp 'croak';
use Mojo::Base 'Mojo::EventEmitter';
use Mojo::IOLoop;
use Mojo::Util qw/b64_encode b64_decode/;
use Authen::SASL;
use Text::Eggrpc;

$VERSION = '0.1';

# are we authenticated?
has authenticated => 0;

# user supplied things
has client        => undef; 
has password      => undef;
has sasl_callback => undef;
has sasl_mech     => undef;

# queue of callbacks waiting for their responses to be delivered in order
# buffer of incomplete incoming RPC messages
has [qw(buffer queue)];

sub new {
	my $self = shift->SUPER::new(@_);

	$self->{buffer} = [];
	$self->{queue} = [];
	$self->{sasl_mech} = [qw/CRAM-MD5 DIGEST-MD5/] unless $self->{sasl_mech} ;

	return $self;
}

=head1 METHODS

L<Mojo::EggdropRPC> inherits all methods from L<Mojo::EventEmitter> and
implements the following new ones.

=head2 connect

  $rpc->connect(address => '', port => 1234);

Initializes the internal Mojo::IOLoop::Client instance and connects it to the
given address and port. All arguments are passed to Mojo::IOLoop->client.

=cut

sub connect {
	my $self = shift;
	$self->{client} = Mojo::IOLoop->client({@_}, sub {
		my ($loop, $err, $stream) = @_;
		$stream->on(read => sub { $self->receive(@_) });
		$self->{stream} = $stream;
	});
}


# hexdump data to stdout. Used only for debugging. Works only on GNU systems
# because it uses GNU od(1) specific parameters. Replace 'od -flags ...' by
# 'hd' on *BSD.
sub hexdump {
	my $data = shift;
	print "\n";
	open PIPE, '|od -A x -t x1z -v';
	print PIPE $data;
	close PIPE;
}

# internal: send data, completely unaware of callbacks, which are handled by
# call.
sub send {
	my ($self, $data) = @_;
	$data = rpc_format @$data;
	$self->{stream}->write($data);
	$self->emit(sent => $data);
}

=head2 call

  $rpc->call(["some", "rpc", "call"] => sub {
    my ($rpc, $response) = @_;
    croak ":(" if $response->[0] eq 'ERROR';
  });

Sends the method call to the server and calls the callback with the response
asynchronously, as soon as it arrives. The callback's first parameter is the
EggdropRPC object itself, the second parameter is an array containing the
response. The callback may be omitted. Each time data is sent, the "sent" event
is emitted.

=cut

sub call {
	my ($self, $data, $callback) = @_;
	croak "not yet authenticated" unless $self->{authenticated}; # FIXME queue this?
	$callback ||= 0; # in case it was undef...
	push $self->{queue}, $callback;
	$self->send($data);
}

# used internally to authenticate the client to the server before any commands
# can be sent.
sub authenticate {
	my ($self, $data) = @_;

	if ($data->[0] eq "OK") {
		$self->{authenticated} = 1;
		$self->{sasl} = undef;
		$self->emit(authenticated => 1);
		return;
	}

	croak 'No password' unless $self->{password};

	if ($data->[0] eq "AUTHENTICATION" && $data->[1] eq "BEGIN"
	      && $data->[2] eq "plaintext-password") {

		$self->send(['authenticate', $self->{password}]);

	} elsif ($data->[0] eq "AUTHENTICATION" && $data->[1] eq "BEGIN") {
		croak "Server requested disallowed SASL mech $data->[2]"
	       		unless grep { $data->[2] eq $_ } @{$self->{sasl_mech}};

		$self->{sasl} = Authen::SASL->new(
			mech => $data->[2],
			callback => {
				user => "",
				pass => $self->{password}
			}
		)->client_new;

		my $rsp = $self->{sasl}->client_step(b64_decode $data->[3]);
		$rsp = b64_encode $rsp;
		$rsp =~ s/\s//g;
		$self->send(['authenticate', $rsp]);

	} elsif ($data->[0] eq "AUTHENTICATION" && $data->[1] eq "CONTINUE") {

		my $rsp = $self->{sasl}->client_step(b64_decode $data->[2]);
		$rsp = b64_encode $rsp;
		$rsp =~ s/\s//g;
		$self->send(['authenticate', $rsp]);

	} else {
		croak "invalid authentication state";
	}
}

# used in the Mojo::IOLoop::Client callback. Reads, buffers and handles
# messages and invokes the callback corresponding to the RPC call in question.
sub receive {
	my ($self, $stream, $data) = @_;

	push $self->{buffer}, $data;

	return unless rpc_complete $data;

	$self->emit(received => $data);

	my $msg = rpc_split join('', @{$self->{buffer}});
	$self->{buffer} = [];

	unless ($self->{authenticated}) {
		$self->authenticate($msg);
		return;
	}

	if (@{$self->{queue}}) {
		my $cb = shift $self->{queue};
		&$cb($self, $msg) if $cb;
		return;
	}

	$self->emit(error => 'unhandled message');
}

=head1 AUTHOR

Moritz Wilhelmy <mwilhelmy@cpan.org>

The latest development version is available at <https://bitbucket.org/wilhelmy/mojo-eggdroprpc>

=head1 COPYRIGHT

This module is Copyright (C) 2015, Moritz Wilhelmy

You may copy or modify it under the same terms as Perl 5 itself.

=head1 SEE ALSO

L<Mojo::IOLoop>, L<Mojo::EventEmitter>

=cut

1
